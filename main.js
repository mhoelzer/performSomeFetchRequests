function displayQandA(clueInfo) {
    let textHere = document.createTextNode(clueInfo);
    let div = document.createElement("div");
    let main = document.getElementById("main");
    div.appendChild(textHere);
    main.appendChild(div);
};
function findRandomClueIdNumber(max) {
    return Math.floor(Math.random() * max);
};

function findClueUsingFetchAndThen() {
    fetch("http://jservice.io/api/category?id=136")
        .then(response => response.json())
        .then(info => {
            // let clue = info.clues[1]; // put w/e id number you want inside []
            let clue = info.clues[findRandomClueIdNumber(info.clues.length)];
            let question = clue.question;
            let answer = clue.answer;
            displayQandA(`~ Category is ${info.title} and Uses "fetch"/".then" => ${question}: ${answer}. ~`);
        });
}
findClueUsingFetchAndThen();

async function findClueUsingAsync() {
    let response = await fetch("http://jservice.io/api/category?id=67");
    let category = await response.json();
    // let info = await category.clues[1]
    let info = await category.clues[findRandomClueIdNumber(category.clues.length)];
    displayQandA(`~ Category is ${category.title} and Uses "async" => ${info.question}: ${info.answer}. ~`);
}
findClueUsingAsync();